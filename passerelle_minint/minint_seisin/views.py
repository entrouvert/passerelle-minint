# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

import passerelle.utils as utils
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView as GenericDetailView
from passerelle.soap import client_to_jsondict, sudsobject_to_dict

from .models import SeisinManagement
from .soap import get_client


class SeisinManagementDetailView(GenericDetailView):
    model = SeisinManagement
    template_name = 'minint_seisin/detail.html'


class DetailView(GenericDetailView):
    model = SeisinManagement

    def get_client(self):
        return get_client(self.get_object())

    def get_data(self, request, *args, **kwargs):
        raise NotImplementedError

    @utils.protected_api('can_access')
    def get(self, request, *args, **kwargs):
        data = self.get_data(request, *args, **kwargs)
        return utils.response_for_json(request, data)


class PingView(DetailView):
    def get_data(self, request, *args, **kwargs):
        client = self.get_client()
        res = {'ping': 'pong'}
        if 'debug' in request.GET:
            res['client'] = client_to_jsondict(client)
        return res


class ResourceView(DetailView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @utils.protected_api('can_access')
    def post(self, request, *args, **kwargs):
        client = self.get_client()
        formdata = json.loads(request.body)
        fields = formdata.get('fields', {})
        extras = formdata.get('extra', {})
        workflow_fields = formdata.get('workflow', {}).get('fields', {})

        debug = 'debug' in request.GET
        if debug:
            debug_output = {}

        # attachment: only one allowed. Search first in workflow_fields, then in fields
        attachmentData = client.factory.create('arrayOfAttachmentContent')
        attachmentData.source = '?'
        for name, value in workflow_fields.items():
            if isinstance(value, dict) and value.get('filename') and value.get('content'):
                attachmentData.fileName = '"%s"' % value['filename']
                attachmentData.attachmentType = value.get('content_type')
                attachmentData.source = value['content']
                break

        if not attachmentData.fileName:
            for name, value in fields.items():
                if isinstance(value, dict) and value.get('filename') and value.get('content'):
                    attachmentData.fileName = '"%s"' % value['filename']
                    attachmentData.attachmentType = value.get('content_type')
                    attachmentData.source = value['content']
                    break

        if debug:
            debug_output['attachmentData'] = '%r' % attachmentData

        # data
        data = client.factory.create('listOfData')
        data.datas = []
        for name, value in extras.items():
            if not name.startswith('seisin_by_email_data_'):
                continue
            item = client.factory.create('arrayOfDataContent')
            item.key = name[21:]
            item.value = value
            data.datas.append(item)

        if debug:
            debug_output['data'] = '%r' % data

        # other variables
        departement_number = extras.get('seisin_by_email_departement_number', '')
        departement_numbers = extras.get('seisin_by_email_departement_numbers', False)
        if departement_numbers:
            #  without duplicates
            departement_numbers = list(dict.fromkeys(departement_numbers.split(',')))
        target_entity = extras.get('seisin_by_email_target_entity', -1)
        theme = extras.get('seisin_by_email_theme', '')
        userType = extras.get('seisin_by_email_userType', '')
        request_object = extras.get('seisin_by_email_request_object', '')

        if debug:
            debug_output['departement_number'] = departement_number
            debug_output['departement_numbers'] = departement_numbers
            debug_output['target_entity'] = target_entity
            debug_output['theme'] = theme
            debug_output['userType'] = userType
            debug_output['request_object'] = request_object

        # call sendSeisinByEmail web service
        if departement_number:
            results = client.service.sendSeisinByEmail(
                departement_number, target_entity, theme, userType, request_object, data, attachmentData
            )
            data = sudsobject_to_dict(results)
        elif departement_numbers:
            results = []
            for dep in departement_numbers:
                # dep may be empty
                if dep:
                    result = client.service.sendSeisinByEmail(
                        dep, target_entity, theme, userType, request_object, data, attachmentData
                    )
                    results.append(sudsobject_to_dict(result))
            data = {}
            data['results'] = results
        if debug:
            data['debug'] = debug_output

        return utils.response_for_json(request, data)
