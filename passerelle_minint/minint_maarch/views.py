# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import json
import logging
import re
import urllib.parse
from datetime import datetime

import passerelle.utils as utils
import requests
from django.conf import settings
from django.utils.decorators import method_decorator
from django.utils.encoding import force_str
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView as GenericDetailView
from passerelle.base.signature import sign_url
from passerelle.soap import client_to_jsondict, sudsobject_to_dict
from suds import TypeNotFound

from .models import MinIntMaarch
from .soap import get_client

logger = logging.getLogger('minint_maarch')


def get_schema(formdata):
    # get formdef schema from wcs API
    url = formdata['url']
    p = urllib.parse.urlsplit(url)
    scheme, netloc, path, query, fragment = p.scheme, p.netloc, p.path, p.query, p.fragment
    schema_path = path.rsplit('/', 2)[0] + '/schema'
    schema_url = urllib.parse.urlunsplit((scheme, netloc, schema_path, query, fragment))
    if getattr(settings, 'KNOWN_SERVICES', {}).get('wcs'):
        wcs_service = list(settings.KNOWN_SERVICES['wcs'].values())[0]
        schema_url += '?orig=%s' % wcs_service.get('orig')
        schema_url = sign_url(schema_url, key=wcs_service.get('secret'))
    return requests.get(schema_url).json()


class MaarchException(Exception):
    pass


class MaarchDetailView(GenericDetailView):
    model = MinIntMaarch
    template_name = 'minint_maarch/detail.html'


class DetailView(GenericDetailView):
    model = MinIntMaarch
    translations = False

    def get_client(self):
        return get_client(self.get_object())

    def get_data(self, request, *args, **kwargs):
        raise NotImplementedError

    @utils.protected_api('can_access')
    def get(self, request, *args, **kwargs):
        data = self.get_data(request, *args, **kwargs)
        return utils.response_for_json(request, data)

    def prepare_regexp_filename_replacement(self, extras):
        pattern = extras.get('filename_search_pattern_in_label')
        result = extras.get('filename_replace_pattern')
        if pattern and result:
            return re.compile(pattern), result
        return False, False

    def translate(self, key, default):
        return self.translations[key] if self.translations and key in self.translations else default


class PingView(DetailView):
    def get_data(self, request, *args, **kwargs):
        client = self.get_client()
        res = {'ping': 'pong'}
        if 'debug' in request.GET:
            res['client'] = client_to_jsondict(client)
        return res


class ResourceView(DetailView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @utils.protected_api('can_access')
    def post(self, request, *args, **kwargs):
        client = self.get_client()
        formdata = json.loads(request.body)
        extras = formdata.get('extra', {})

        debug = 'debug' in request.GET
        if debug:
            debug_output = {}

        schema = get_schema(formdata)

        # storeAttachmentResource attachments: list, build from formdata file fields
        attachments = []

        # regexp to rename attachments
        (
            regexp_filename_search_pattern_in_label,
            filename_result_pattern,
        ) = self.prepare_regexp_filename_replacement(extras)

        #
        # build document (encodedFile and fileFormat)
        #

        # get translations
        translations_txt = extras.get('translations')
        if translations_txt:
            self.translations = json.loads(translations_txt)
            # logger.debug('Translation JSON: %r', translations)

        document = '<html>'
        document += '<head><meta charset="utf-8"></head>'
        document += '<body>'
        document += '<h1>%s</h1>' % self.translate('form_title', schema['name'])
        page = ''
        empty_page = True
        for field in schema['fields']:
            if field['type'] == 'page':
                # add last page, if it contains values
                if page and not empty_page:
                    document += page
                page = '<hr /><h2>%s</h2>' % self.translate(field['label'], field['label'])
                empty_page = True
            elif field['type'] == 'title':
                page += '<h3>%s</h3>' % field['label']
            # elif field['type'] == 'subtitle':
            #     page += '<h3>%s</h3>' % self.translate(field['label'], field['label'])
            elif 'varname' in field:
                varname = field['varname']
                value = formdata['fields'].get(varname)
                if not value:
                    continue
                if field['type'] == 'file':
                    # field is a file: add it to attachments list
                    value['fileFormat'] = value['content_type'].split('/')[1]  # FIXME (how ?)
                    if regexp_filename_search_pattern_in_label:
                        m = regexp_filename_search_pattern_in_label.match(field['label'])
                        if m:
                            value['filename_original'] = value['filename']
                            value['filename'] = filename_result_pattern.replace(
                                'searchedPatternInLabel', m.group(1)
                            )
                            if 'filename' in filename_result_pattern:
                                value['filename'] = value['filename'].replace(
                                    'filename', value['filename_original']
                                )
                    attachments.append(value)
                    value = '%s' % value['filename']
                elif field['type'] == 'date':
                    value = datetime.strptime(value, '%Y-%m-%d')
                    value = value.strftime('%d-%m-%Y')
                elif field['type'] == 'item' and self.translations:
                    value_raw = formdata['fields'].get(varname + '_raw')
                    value = self.translate(varname + '_' + value_raw, value)
                page += '<dl>'
                page += '<dt>%s</dt>' % self.translate(varname, field['label'])
                page += '<dd>%s</dd>' % value
                page += '</dl>'
                empty_page = False
        if page and not empty_page:  # add last page, if it contains values
            document += page
        document += '</body></html>'
        encodedFile = force_str(base64.b64encode(document.encode('utf-8')))
        fileFormat = 'html'

        if debug:
            debug_output['document'] = document

        #
        # build metadata for storeResource and storeExtResource
        #

        # storeResource metadata
        metadata = {}
        # storeExtResource metadata
        ext_metadata = {}
        # createContact data
        contact = {}
        contact_table1_pp = {'title', 'lastname', 'firstname', 'contact_type', 'is_corporate_person'}

        # extract metadata and ext_metadata from formdata['extra']
        for name, value in extras.items():
            if name.startswith('maarch_contact_'):
                contact[name[15:]] = value
            if name.startswith('maarch_metadata_'):
                metadata[name[16:]] = value
            if name.startswith('maarch_ext_metadata_'):
                ext_metadata[name[20:]] = value

        def prepare_soap_contact(contact, contact_table1_pp):
            datas = []
            for name, value in contact.items():
                data = client.factory.create('arrayOfDataContactContent')
                data.column = name
                if name.endswith('_date'):
                    data.type = 'date'
                    value = datetime.strptime(value[:19], '%Y-%m-%dT%H:%M:%S')
                    value = value.strftime('%d-%m-%Y %H:%M:%S')
                elif isinstance(value, str):
                    data.type = 'string'
                elif isinstance(value, int):
                    data.type = 'integer'
                elif isinstance(value, float):
                    data.type = 'float'
                elif value is None:
                    data.type = 'string'
                    value = ''
                data.value = value
                if name in contact_table1_pp:
                    data.table = 'contacts_v2'
                else:
                    data.table = 'contact_addresses'
                datas.append(data)
            soap_contact = client.factory.create('arrayOfDataContact')
            soap_contact.datas = datas
            return soap_contact

        # prepare metadata for SOAP call
        def prepare_soap_metadata(metadata):
            datas = []
            for name, value in metadata.items():
                data = client.factory.create('arrayOfDataContent')
                data.column = name
                if name.endswith('_date'):
                    data.type = 'date'
                    value = datetime.strptime(value[:19], '%Y-%m-%dT%H:%M:%S')
                    value = value.strftime('%d-%m-%Y %H:%M:%S')
                elif isinstance(value, str):
                    data.type = 'string'
                elif isinstance(value, int):
                    data.type = 'int'
                elif isinstance(value, float):
                    data.type = 'float'
                elif value is None:
                    data.type = 'string'
                    value = ''
                data.value = value
                datas.append(data)
            soap_metadata = client.factory.create('arrayOfData')
            soap_metadata.datas = datas
            return soap_metadata

        logger.debug('Extraction DATA contact %r: ', contact)
        logger.debug('Extraction DATA metadata %r: ', metadata)

        # if INES ESB (Tibco) between passerelle and Maarch, add a "maarch_id"
        # parameter. Get value from formdata or workflow options.
        maarch_id = extras.get('maarch_id')

        if maarch_id:
            contact = False
        else:
            contact = prepare_soap_contact(contact, contact_table1_pp)

        metadata['nbpj'] = len(attachments)
        metadata = prepare_soap_metadata(metadata)

        if debug:
            debug_output['contact'] = '%r' % contact
            debug_output['metadata'] = '%r' % metadata

        #
        # get other Maarch variables (letterbox configuration by default)
        #

        collId = extras.get('maarch_collId') or 'letterbox_coll'
        table = extras.get('maarch_table') or 'res_letterbox'
        status = extras.get('maarch_status') or 'ATT'
        ext_table = extras.get('maarch_ext_table') or 'mlb_coll_ext'

        if debug:
            debug_output['collId'] = collId
            debug_output['table'] = table
            debug_output['status'] = status
            debug_output['ext_table'] = ext_table

        if debug:
            debug_output['maarch_id'] = maarch_id

        #
        # call Maarch web services
        #
        logger.debug('createContact: start')

        if maarch_id:
            logger.debug('createContact : SKIPPED done by INES for maarch_id: %r', maarch_id)
            data_contact = False
        else:
            try:
                create_contact_result = client.service.CreateContact(contact)
                data_contact = sudsobject_to_dict(create_contact_result)
                logger.debug('createContact result: %r', data_contact)
            except TypeNotFound:
                pass

        logger.debug('createContact: end')
        logger.debug('storeResource+Ext+Attachment: start')

        # Prepare ext_metadata
        if data_contact:
            ext_metadata['exp_contact_id'] = data_contact['contactId']
            ext_metadata['address_id'] = data_contact['addressId']

        ext_metadata = prepare_soap_metadata(ext_metadata)
        logger.debug('Extraction DATA ext_metadata %r: ', ext_metadata)
        if debug:
            debug_output['ext_metadata'] = '%r' % ext_metadata

        # store the resource (storeResource)
        logger.debug(
            'storeResource: encodedFile(size):%r fileFormat:%r ' 'collId:%r table:%r status:%r',
            len(encodedFile),
            fileFormat,
            collId,
            table,
            status,
        )
        logger.debug('storeResource: metadata: %r', metadata)
        if maarch_id:
            logger.debug('storeResource: INES maarch_id: %r', maarch_id)
            results = client.service.storeResource(
                maarch_id, encodedFile, metadata, collId, table, fileFormat, status
            )
        else:
            results = client.service.storeResource(encodedFile, metadata, collId, table, fileFormat, status)
        data = sudsobject_to_dict(results)
        logger.debug('storeResource result: %r', data)

        resId = data.get('resId')
        if not resId:
            raise MaarchException('no resId after storeResource')
        logger.debug('storeResource result: resId:%r', resId)

        logger.debug('storeExtResource: resId:%r ext_table:%r', resId, ext_table)
        # logger.debug('storeExtResource: ext_metadata: %r', ext_metadata)
        # store external metadata (storeExtResource)
        if maarch_id:
            logger.debug('storeExtResource: INES maarch_id: %r', maarch_id)
            results = client.service.storeExtResource(maarch_id, resId, ext_metadata, ext_table)
        else:
            results = client.service.storeExtResource(resId, ext_metadata, ext_table)

        # store attachments
        for attachment in attachments:
            logger.debug(
                'storeAttachmentResource: resId:%r collId:%r ' 'content(size):%r fileFormat:%r filename:%r',
                resId,
                collId,
                len(attachment['content']),
                attachment['fileFormat'],
                attachment['filename'],
            )
            if maarch_id:
                logger.debug('storeAttachmentResource: INES maarch_id: %r', maarch_id)
                client.service.storeAttachmentResource(
                    maarch_id,
                    resId,
                    collId,
                    attachment['content'],
                    attachment['fileFormat'],
                    attachment['filename'],
                )
            else:
                client.service.storeAttachmentResource(
                    resId, collId, attachment['content'], attachment['fileFormat'], attachment['filename']
                )

        if debug:
            data['debug'] = debug_output

        logger.debug('storeResource+Ext+Attachment: resId:%r -- end', resId)
        return utils.response_for_json(request, data)


class AttachmentView(DetailView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @utils.protected_api('can_access')
    def post(self, request, *args, **kwargs):
        client = self.get_client()
        formdata = json.loads(request.body)
        extras = formdata.get('extra', {})

        debug = 'debug' in request.GET
        if debug:
            debug_output = {}

        schema = get_schema(formdata)

        (
            regexp_filename_search_pattern_in_label,
            filename_result_pattern,
        ) = self.prepare_regexp_filename_replacement(extras)

        # storeAttachmentResource attachments: list, build from formdata file fields
        attachments = []
        for field in schema['fields']:
            if 'varname' in field:
                varname = field['varname']
                value = formdata['fields'].get(varname)
                if not value:
                    continue
                if field['type'] == 'file':
                    # field is a file: add it to attachments list
                    value['fileFormat'] = value['content_type'].split('/')[1]  # FIXME (how ?)
                    if regexp_filename_search_pattern_in_label:
                        m = regexp_filename_search_pattern_in_label.match(field['label'])
                        if m:
                            value['filename_original'] = value['filename']
                            value['filename'] = filename_result_pattern.replace(
                                'searchedPatternInLabel', m.group(1)
                            )
                            if 'filename' in filename_result_pattern:
                                value['filename'] = value['filename'].replace(
                                    'filename', value['filename_original']
                                )
                    attachments.append(value)
        #
        # get other Maarch variables (letterbox configuration by default)
        #
        resId = extras.get('maarch_resId') or 0
        collId = extras.get('maarch_collId') or 'letterbox_coll'

        data = {}

        if debug:
            debug_output['resId'] = resId
            debug_output['collId'] = collId

        # if INES ESB (Tibco) between passerelle and Maarch, add a "maarch_id"
        # parameter. Get value from formdata or workflow options.
        maarch_id = extras.get('maarch_id')
        notification_email = extras.get('notification_email') if maarch_id else None
        notification_model = extras.get('notification_model') if maarch_id else None

        if debug:
            debug_output['maarch_id'] = maarch_id

        logger.debug('storeAttachment: start')

        if debug:
            debug_output['res'] = []

        # store attachments
        nbAttachments = len(attachments)
        countAttachment = 0
        for attachment in attachments:
            logger.debug(
                'storeAttachmentResource: resId:%r collId:%r ' 'content(size):%r fileFormat:%r filename:%r',
                resId,
                collId,
                len(attachment['content']),
                attachment['fileFormat'],
                attachment['filename'],
            )
            countAttachment += 1
            if maarch_id:
                email_to = (
                    notification_email if notification_email and countAttachment == nbAttachments else ''
                )
                modele_ae = (
                    notification_model if notification_model and countAttachment == nbAttachments else ''
                )
                logger.debug('storeAdditionalAttachmentResource: INES maarch_id: %r', maarch_id)
                client.service.storeAdditionalAttachmentResource(
                    maarch_id,
                    resId,
                    collId,
                    attachment['content'],
                    attachment['fileFormat'],
                    attachment['filename'],
                    email_to,
                    modele_ae,
                )
            else:
                results = client.service.storeAttachmentResource(
                    resId, collId, attachment['content'], attachment['fileFormat'], attachment['filename']
                )
                res = sudsobject_to_dict(results)
                if debug:
                    debug_output['res'].append(res)

        if debug:
            data['debug'] = debug_output
            logger.debug('debug : %r', data['debug'])
        logger.debug('storeAttachment: resId:%r -- end', resId)
        return utils.response_for_json(request, data)


class DebugView(DetailView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @utils.protected_api('can_access')
    def post(self, request, *args, **kwargs):
        client = self.get_client()
        formdata = json.loads(request.body)
        extras = formdata.get('extra', {})
        logger.debug('formdata %r: ', formdata)
        debug = False  # 'debug' in request.GET
        if debug:
            debug_output = {}

        schema = get_schema(formdata)

        # storeAttachmentResource attachments: list, build from formdata file fields
        attachments = []

        # regexp to rename attachments
        (
            regexp_filename_search_pattern_in_label,
            filename_result_pattern,
        ) = self.prepare_regexp_filename_replacement(extras)

        #
        # build document (encodedFile and fileFormat)
        #

        translations_txt = extras.get('translations')

        if translations_txt:
            self.translations = json.loads(translations_txt)
            logger.debug('Translation JSON: %r', self.translations)
        else:
            logger.debug('AUCUNE Translation JSON: %r', self.translations)

        document = '<html>'
        document += '<head><meta charset="utf-8"></head>'
        document += '<body>'
        document += '<h1>%s</h1>' % self.translate('form_title', schema['name'])
        page = ''
        empty_page = True
        for field in schema['fields']:
            logger.debug('field type %r: ', field['type'])
            if field['type'] == 'page':
                # add last page, if it contains values
                if page and not empty_page:
                    document += page
                page = '<hr /><h2>%s</h2>' % self.translate(field['label'], field['label'])
                empty_page = True
            # elif field['type'] == 'title':
            #     page += '<h3>%s</h3>' % self.translate(field['label'], field['label'])
            elif field['type'] == 'subtitle':
                page += '<h3>%s</h3>' % self.translate(field['label'], field['label'])
            elif 'varname' in field:
                varname = field['varname']
                value = formdata['fields'].get(varname)
                if not value:
                    continue
                if field['type'] == 'file':
                    # field is a file: add it to attachments list
                    value['fileFormat'] = value['content_type'].split('/')[1]  # FIXME (how ?)
                    if regexp_filename_search_pattern_in_label:
                        m = regexp_filename_search_pattern_in_label.match(field['label'])
                        if m:
                            value['filename_original'] = value['filename']
                            value['filename'] = filename_result_pattern.replace(
                                'searchedPatternInLabel', m.group(1)
                            )
                            if 'filename' in filename_result_pattern:
                                value['filename'] = value['filename'].replace(
                                    'filename', value['filename_original']
                                )
                    attachments.append(value)
                    value = '%s' % value['filename']
                elif field['type'] == 'date':
                    value = datetime.strptime(value, '%Y-%m-%d')
                    value = value.strftime('%d-%m-%Y')
                elif field['type'] == 'item' and self.translations:
                    value_raw = formdata['fields'].get(varname + '_raw')
                    value = self.translate(varname + '_' + value_raw, value)
                page += '<dl>'
                page += '<dt>%s</dt>' % self.translate(varname, field['label'])
                page += '<dd>%s</dd>' % value
                page += '</dl>'
                empty_page = False
        if page and not empty_page:  # add last page, if it contains values
            document += page
        document += '</body></html>'
        logger.debug('DOCUMENT HTML %r ', document)
        encodedFile = force_str(base64.b64encode(document.encode('utf-8')))
        fileFormat = 'html'

        if debug:
            debug_output['document'] = document

        #
        # build metadata for storeResource and storeExtResource
        #

        # storeResource metadata
        metadata = {}
        # storeExtResource metadata
        ext_metadata = {}
        # createContact data
        contact = {}
        contact_table1_pp = {'title', 'lastname', 'firstname', 'contact_type', 'is_corporate_person'}

        # extract metadata and ext_metadata from formdata['extra']
        for name, value in extras.items():
            if name.startswith('maarch_contact_'):
                contact[name[15:]] = value
            if name.startswith('maarch_metadata_'):
                metadata[name[16:]] = value
            if name.startswith('maarch_ext_metadata_'):
                ext_metadata[name[20:]] = value

        def prepare_soap_contact(contact, contact_table1_pp):
            datas = []
            for name, value in contact.items():
                data = client.factory.create('arrayOfDataContactContent')
                data.column = name
                if name.endswith('_date'):
                    data.type = 'date'
                    value = datetime.strptime(value[:19], '%Y-%m-%dT%H:%M:%S')
                    value = value.strftime('%d-%m-%Y %H:%M:%S')
                elif isinstance(value, str):
                    data.type = 'string'
                elif isinstance(value, int):
                    data.type = 'integer'
                elif isinstance(value, float):
                    data.type = 'float'
                elif value is None:
                    data.type = 'string'
                    value = ''
                data.value = value
                if name in contact_table1_pp:
                    data.table = 'contacts_v2'
                else:
                    data.table = 'contact_addresses'
                datas.append(data)
            soap_contact = client.factory.create('arrayOfDataContact')
            soap_contact.datas = datas
            return soap_contact

        # prepare metadata for SOAP call
        def prepare_soap_metadata(metadata):
            datas = []
            for name, value in metadata.items():
                data = client.factory.create('arrayOfDataContent')
                data.column = name
                if name.endswith('_date'):
                    data.type = 'date'
                    value = datetime.strptime(value[:19], '%Y-%m-%dT%H:%M:%S')
                    value = value.strftime('%d-%m-%Y %H:%M:%S')
                elif isinstance(value, str):
                    data.type = 'string'
                elif isinstance(value, int):
                    data.type = 'int'
                elif isinstance(value, float):
                    data.type = 'float'
                elif value is None:
                    data.type = 'string'
                    value = ''
                data.value = value
                datas.append(data)
            soap_metadata = client.factory.create('arrayOfData')
            soap_metadata.datas = datas
            return soap_metadata

        # logger.debug('Extraction DATA contact %r: ', contact)
        # logger.debug('Extraction DATA metadata %r: ', metadata)

        # if INES ESB (Tibco) between passerelle and Maarch, add a "maarch_id"
        # parameter. Get value from formdata or workflow options.
        maarch_id = extras.get('maarch_id')

        if maarch_id:
            contact = False
        else:
            contact = prepare_soap_contact(contact, contact_table1_pp)

        metadata['nbpj'] = len(attachments)
        metadata = prepare_soap_metadata(metadata)

        if debug:
            debug_output['contact'] = '%r' % contact
            debug_output['metadata'] = '%r' % metadata

        #
        # get other Maarch variables (letterbox configuration by default)
        #

        collId = extras.get('maarch_collId') or 'letterbox_coll'
        table = extras.get('maarch_table') or 'res_letterbox'
        status = extras.get('maarch_status') or 'ATT'
        ext_table = extras.get('maarch_ext_table') or 'mlb_coll_ext'

        if debug:
            debug_output['collId'] = collId
            debug_output['table'] = table
            debug_output['status'] = status
            debug_output['ext_table'] = ext_table

        if debug:
            debug_output['maarch_id'] = maarch_id

        #
        # call Maarch web services
        #
        logger.debug('createContact: start')

        if maarch_id:
            logger.debug('createContact : SKIPPED done by INES for maarch_id: %r', maarch_id)
            data_contact = False
        else:
            try:
                # create_contact_result = client.service.CreateContact(contact)
                # data_contact = sudsobject_to_dict(create_contact_result)
                data_contact = False
                logger.debug('createContact result: %r', data_contact)
            except TypeNotFound:
                pass

        logger.debug('createContact: end')
        logger.debug('storeResource+Ext+Attachment: start')

        # Prepare ext_metadata
        if data_contact:
            ext_metadata['exp_contact_id'] = data_contact['contactId']
            ext_metadata['address_id'] = data_contact['addressId']

        ext_metadata = prepare_soap_metadata(ext_metadata)
        # logger.debug('Extraction DATA ext_metadata %r: ', ext_metadata)
        if debug:
            debug_output['ext_metadata'] = '%r' % ext_metadata

        # store the resource (storeResource)
        logger.debug(
            'storeResource: encodedFile(size):%r fileFormat:%r ' 'collId:%r table:%r status:%r',
            len(encodedFile),
            fileFormat,
            collId,
            table,
            status,
        )
        # logger.debug('storeResource: metadata: %r', metadata)
        if maarch_id:
            logger.debug('storeResource: INES maarch_id: %r', maarch_id)
            # results = client.service.storeResource(
            #     maarch_id,
            #     encodedFile, metadata, collId,
            #     table, fileFormat, status)
        # else:
        # results = client.service.storeResource(
        #     encodedFile, metadata, collId,
        #     table, fileFormat, status)

        # results = {'err': 0, 'data': {'returnCode': 0, 'resId': 1000}}

        # data = sudsobject_to_dict(results)
        # logger.debug('storeResource result: %r', data)

        resId = 1000  # data.get('resId')
        if not resId:
            raise MaarchException('no resId after storeResource')
        logger.debug('storeResource result: resId:%r', resId)

        # logger.debug('storeExtResource: resId:%r ext_table:%r', resId,
        #              ext_table)
        # logger.debug('storeExtResource: ext_metadata: %r', ext_metadata)
        # store external metadata (storeExtResource)
        if maarch_id:
            logger.debug('storeExtResource: INES maarch_id: %r', maarch_id)
            # results = client.service.storeExtResource(
            #     maarch_id,
            #     resId, ext_metadata, ext_table)
        # else:
        # results = client.service.storeExtResource(
        #     resId, ext_metadata, ext_table)
        results = {'returnCode': 0, 'resId': 1000}

        # store attachments
        for attachment in attachments:
            logger.debug(
                'storeAttachmentResource: resId:%r collId:%r content(size):%r fileFormat:%r filename:%r',
                resId,
                collId,
                len(attachment['content']),
                attachment['fileFormat'],
                attachment['filename'],
            )
            if maarch_id:
                logger.debug('storeAttachmentResource: INES maarch_id: %r', maarch_id)
                # client.service.storeAttachmentResource(
                #     maarch_id,
                #     resId, collId, attachment['content'],
                #     attachment['fileFormat'], attachment['filename'])
            # else:
            # client.service.storeAttachmentResource(
            #     resId, collId, attachment['content'],
            #     attachment['fileFormat'], attachment['filename'])

        data = {}
        data['resId'] = 1000
        if debug:
            data['debug'] = debug_output

        logger.debug('storeResource+Ext+Attachment: resId:%r -- end', resId)
        return utils.response_for_json(request, data)
