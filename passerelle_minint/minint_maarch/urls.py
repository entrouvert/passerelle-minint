# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import re_path

from .views import *

urlpatterns = [
    re_path(r'^(?P<slug>[\w,-]+)/$', MaarchDetailView.as_view(), name='minint-maarch-view'),
    re_path(r'^(?P<slug>[\w,-]+)/ping/$', PingView.as_view(), name='minint-maarch-ping'),
    re_path(r'^(?P<slug>[\w,-]+)/resource/$', ResourceView.as_view(), name='minint-maarch-resource'),
    re_path(r'^(?P<slug>[\w,-]+)/attachment/$', AttachmentView.as_view(), name='minint-maarch-attachment'),
    re_path(r'^(?P<slug>[\w,-]+)/debug/$', DebugView.as_view(), name='minint-maarch-debug'),
]
