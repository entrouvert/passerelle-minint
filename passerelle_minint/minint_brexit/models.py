from urllib.parse import quote

import requests
from django.db import models
from django.utils.translation import gettext_lazy as _
from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError, exception_to_text


class Brexit(BaseResource):
    category = 'MinInt'
    api_description = "Ce connecteur permet de vérifier l'existence d'un dossier dans la GED Etranger utilisée pour le BREXIT."

    url = models.URLField('URL', default='IP_INES/action')

    class Meta:
        verbose_name = 'BREXIT - API REST vers MAARCH'

    @endpoint()
    def info(self, request):
        return {'hello': 'world'}

    @endpoint(
        parameters={
            'num': {'description': _('Num dossier'), 'example_value': '12345'},
            'email': {
                'description': _('Email associe au dossier'),
                'example_value': 'franck@interieur.gouv.fr',
            },
        }
    )
    def find_folder(self, request, num, email):

        self.logger.info('MININT BREXIT - find_folder num: %s  ; email : %s', num, email)

        try:
            response = self.requests.get(self.url + '/%s/%s' % (num, email), verify=False)
        except requests.RequestException as e:
            raise APIError('API-maarch connection error: %s' % exception_to_text(e), data=[])
        try:
            data = response.json()
        except ValueError as e:
            content = repr(response.content[:1000])
            raise APIError(
                'API-maarch returned non-JSON content with status %s: %s' % (response.status_code, content),
                data={
                    'status_code': response.status_code,
                    'exception': exception_to_text(e),
                    'content': content,
                },
            )
        if response.status_code != 200:
            if data.get('error') == 'not_found':
                return {
                    'err': 1,
                    'err_desc': data.get('message', 'not-found'),
                }
            raise APIError(
                'API-maarch returned a non 200 status %s: %s' % (response.status_code, data),
                data={
                    'status_code': response.status_code,
                    'content': data,
                },
            )
        document_id = data.get('res_id')
        if document_id is not None:
            data_response = {'res_id': document_id}
        else:
            data_response = {'res_id': 0, 'result': data.get('result'), 'msg': data.get('error')}
        return {
            'err': 0,
            'data': data_response,
        }
