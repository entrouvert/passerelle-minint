if 'passerelle_minint.minint_maarch' not in INSTALLED_APPS:
    INSTALLED_APPS += ('passerelle_minint.minint_maarch',)
    TENANT_APPS += ('passerelle_minint.minint_maarch',)
if 'passerelle_minint.minint_seisin' not in INSTALLED_APPS:
    INSTALLED_APPS += ('passerelle_minint.minint_seisin',)
    TENANT_APPS += ('passerelle_minint.minint_seisin',)
if 'passerelle_minint.minint_brexit' not in INSTALLED_APPS:
    INSTALLED_APPS += ('passerelle_minint.minint_brexit',)
    TENANT_APPS += ('passerelle_minint.minint_brexit',)
