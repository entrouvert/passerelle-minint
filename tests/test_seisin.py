import utils

from passerelle_minint.minint_seisin.models import SeisinManagement


def test_creation(app, db):
    resource = utils.make_resource(
        SeisinManagement, wsdl_url='https://example.com/', slug='slug', verify_cert=True
    )
