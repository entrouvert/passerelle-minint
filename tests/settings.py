import os

INSTALLED_APPS += (
    'passerelle_minint.minint_maarch',
    'passerelle_minint.minint_seisin',
    'passerelle_minint.minint_brexit',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'TEST': {
            'NAME': 'passerelle-minint-test-%s' % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:63],
        },
    }
}
