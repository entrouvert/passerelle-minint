import utils

from passerelle_minint.minint_maarch.models import MinIntMaarch


def test_creation(app, db):
    resource = utils.make_resource(
        MinIntMaarch, wsdl_url='https://example.com/', slug='slug', verify_cert=True
    )
