from io import BytesIO

import django_webtest
import pytest
from django.core.cache import cache
from django.core.files import File
from httmock import HTTMock, remember_called, response, urlmatch
from utils import make_resource


@pytest.fixture(autouse=True)
def media(settings, tmpdir):
    settings.MEDIA_ROOT = str(tmpdir.mkdir('media'))


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    cache.clear()
    return django_webtest.DjangoTestApp()


@pytest.fixture
def endpoint_dummy_cache(monkeypatch):
    import passerelle.views
    from django.core.cache import caches

    monkeypatch.setattr(passerelle.views, 'cache', caches['dummy'])


@urlmatch()
def internal_server_error(url, request):
    return response(500, 'Internal server error')


@pytest.fixture
def mock_500():
    with HTTMock(internal_server_error):
        yield None
