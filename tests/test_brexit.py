import utils

from passerelle_minint.minint_brexit.models import Brexit


def test_creation(app, db):
    resource = utils.make_resource(Brexit, url='https://example.com/', slug='brexit')
